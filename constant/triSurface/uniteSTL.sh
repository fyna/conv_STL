#!/bin/bash

TARGET=geometry.stl
INLET_SOURCE=inlet.stl
OUTLET_SOURCE=outlet.stl
WALL_SOURCE=walls.stl
MEMBRANE_SOURCE=membr.stl
PB1_SOURCE=pb1.stl
PB2_SOURCE=pb2.stl

[ -f $TARGET	 ] && rm $TARGET
touch $TARGET

sed -i '1 s/^.*$/solid inlet/' $INLET_SOURCE
cat $INLET_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid outlet/' $OUTLET_SOURCE
cat $OUTLET_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid wall/' $WALL_SOURCE
cat $WALL_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid membrane/' $MEMBRANE_SOURCE
cat $MEMBRANE_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid pb1/' $PB1_SOURCE
cat $PB1_SOURCE >> $TARGET

sed -i '1 s/^.*$/solid pb2/' $PB2_SOURCE
cat $PB2_SOURCE >> $TARGET
